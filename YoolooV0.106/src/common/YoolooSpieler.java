// History of Change
// vernr    |date  | who | lineno | what
//  V0.106  |200107| cic |    -   | add history of change

package common;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Scanner;

import common.YoolooKartenspiel.Kartenfarbe;

public class YoolooSpieler implements Serializable {

	private static final long serialVersionUID = 376078630788146549L;
	private String name;
	private Kartenfarbe spielfarbe;
	private int clientHandlerId = -1;
	private int punkte;
	private YoolooKarte[] aktuelleSortierung;
	private String spielstr;
	private int Karte;

	public YoolooSpieler(String name, int maxKartenWert) {
		this.name = name;
		this.punkte = 0;
		this.spielfarbe = null;
		this.aktuelleSortierung = new YoolooKarte[maxKartenWert];
	}
		
	public void sortierungFestlegen() {
		
		System.out.println("Möchten Sie die Kartenreihenfolge beibehalten(b), manuell sortieren (m)oder Strategie anwenden(s)?: ");
		Scanner sc = new Scanner(System.in);
		spielstr = sc.next();
	
		switch(spielstr) {
		case "s":
			  YoolooKarte[] neueSortierung = new YoolooKarte[this.aktuelleSortierung.length];
		      int div = 3;
		      for (int i = 0; i <= div; i++) {
			  int neuerIndex = (int) (Math.random() * (div+1));
			
			  while (neueSortierung[neuerIndex] != null) {
				     neuerIndex = (int) (Math.random() * (div+1));
			         }
			         neueSortierung[neuerIndex] = aktuelleSortierung[i];
			// System.out.println(i+ ". neuerIndex: "+neuerIndex);
		        }
		    for (int i = div+1; i <= 2*div; i++) {
		    	
			int neuerIndex = (int) (Math.random() * div+2*div+1);
			
			while (neueSortierung[neuerIndex] != null) {
				
				neuerIndex = (int) (Math.random() * div+2*div+1);
			}
			neueSortierung[neuerIndex] = aktuelleSortierung[i];
			//System.out.println(i+ ". neuerIndex: "+neuerIndex);
		    }
		    for (int i = neueSortierung.length-div; i < neueSortierung.length; i++) {
			int neuerIndex = (int) (Math.random() * div+div+1);
			
			while (neueSortierung[neuerIndex] != null) {
				neuerIndex = (int) (Math.random() * div+div+1);
			}
			neueSortierung[neuerIndex] = aktuelleSortierung[i];
		    }
		    aktuelleSortierung = neueSortierung;
		    break;
		
		case "b": break;
		case "m": 
						
			for (int i =0; i< this.aktuelleSortierung.length;i++)
			{
				System.out.println("Karte "+(i+1)+": ");
				//Scanner sck = new Scanner(System.in);
				 Karte = sc.nextInt();
				 				
				 aktuelleSortierung[i].setWert(Karte) ;
			 
			} 
			 sc.close();
			break;
		default:
            System.out.println("Keine gültige Auswahl"); 	
            sortierungFestlegen();
            break;
		}
			
	}

	public int erhaeltPunkte(int neuePunkte) {
		System.out.print(name + " hat " + punkte + " P - erhaelt " + neuePunkte + " P - neue Summe: ");
		this.punkte = this.punkte + neuePunkte;
		System.out.println(this.punkte);
		return this.punkte;
	}

	@Override
	public String toString() {
		return "YoolooSpieler [name=" + name + ", spielfarbe=" + spielfarbe + ", puntke=" + punkte
				+ ", altuelleSortierung=" + Arrays.toString(aktuelleSortierung) + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Kartenfarbe getSpielfarbe() {
		return spielfarbe;
	}

	public void setSpielfarbe(Kartenfarbe spielfarbe) {
		this.spielfarbe = spielfarbe;
	}

	public int getClientHandlerId() {
		return clientHandlerId;
	}

	public void setClientHandlerId(int clientHandlerId) {
		this.clientHandlerId = clientHandlerId;
	}

	public int getPunkte() {
		return punkte;
	}

	public void setPunkte(int puntke) {
		this.punkte = puntke;
	}

	public YoolooKarte[] getAktuelleSortierung() {
		return aktuelleSortierung;
	}

	public void setAktuelleSortierung(YoolooKarte[] aktuelleSortierung) {
		for (YoolooKarte karte : aktuelleSortierung) {
			karte.setFarbe(this.spielfarbe);
		}
		this.aktuelleSortierung = aktuelleSortierung;
	}

	public void stichAuswerten(YoolooStich stich) {
		System.out.println(stich.toString());

	}

}
