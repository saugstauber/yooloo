package allgemein;

import client.YoolooClient;
import server.YoolooServer;
import server.YoolooServer.GameMode;

public class StarterTest {

	public static void main(String[] args) {
		int listeningPort = 44137;
		int spieleranzahl = 2; // min 1, max Anzahl definierte Farben in Enum YoolooKartenSpiel.KartenFarbe)
		String hostname = "localhost";
		YoolooServer server = new YoolooServer(listeningPort, spieleranzahl, GameMode.GAMEMODE_SINGLE_GAME);
		YoolooClient clientAlice = new YoolooClient("Alice", hostname, listeningPort);
		YoolooClient clientBob = new YoolooClient("Bob", hostname, listeningPort);
		
		
		server.startServer();
		clientAlice.startClient();
		clientBob.startClient();	
		
	}

}
