// History of Change
// vernr    |date  | who | lineno | what
//  V0.106  |200107| cic |    -   | add history of change 

package server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import common.YoolooKartenspiel;
import common.YoolooKarte;

public class YoolooServer {

	// Server Standardwerte koennen ueber zweite Konstruktor modifiziert werden!
	private int port = 44137;
	private int spielerProRunde = 8; // min 1, max Anzahl definierte Farben in Enum YoolooKartenSpiel.KartenFarbe)
	private GameMode serverGameMode = GameMode.GAMEMODE_SINGLE_GAME;
	public CopyOnWriteArrayList<String> AktuelleSpieler;
	public ConcurrentHashMap<String, YoolooKarte[]> BekannteSpieler;

	public GameMode getServerGameMode() {
		return serverGameMode;
	}

	public void setServerGameMode(GameMode serverGameMode) {
		this.serverGameMode = serverGameMode;
	}

	private ServerSocket serverSocket = null;
	private boolean serverAktiv = true;

	// private ArrayList<Thread> spielerThreads;
	private ArrayList<YoolooClientHandler> clientHandlerList;

	private ExecutorService spielerPool;

	/**
	 * Serverseitig durch ClientHandler angebotenen SpielModi. Bedeutung der
	 * einzelnen Codes siehe Inlinekommentare.
	 * 
	 * Derzeit nur Modus Play Single Game genutzt
	 */
	public enum GameMode {
		GAMEMODE_NULL, // Spielmodus noch nicht definiert
		GAMEMODE_SINGLE_GAME, // Spielmodus: einfaches Spiel
		GAMEMODE_PLAY_ROUND_GAME, // noch nicht genutzt: Spielmodus: Eine Runde von Spielen
		GAMEMODE_PLAY_LIGA, // noch nicht genutzt: Spielmodus: Jeder gegen jeden
		GAMEMODE_PLAY_POKAL, // noch nicht genutzt: Spielmodus: KO System
		GAMEMODE_PLAY_POKAL_LL // noch nicht genutzt: Spielmodus: KO System mit Lucky Looser
	};

	public YoolooServer(int port, int spielerProRunde, GameMode gameMode) {
		this.port = port;
		this.spielerProRunde = spielerProRunde;
		this.serverGameMode = gameMode;
	}

	public void startServer() {
		try {
			// Init
			serverSocket = new ServerSocket(port);
			spielerPool = Executors.newCachedThreadPool();
			clientHandlerList = new ArrayList<YoolooClientHandler>();
			AktuelleSpieler = new CopyOnWriteArrayList<String>();
			BekannteSpieler = getKnownPlayersFromFile();
			System.out.println("Server gestartet - warte auf Spieler");

			while (serverAktiv) {
				Socket client = null;

				// Neue Spieler registrieren
				try {
					client = serverSocket.accept();
					YoolooClientHandler clientHandler = new YoolooClientHandler(this, client);
					clientHandlerList.add(clientHandler);
					System.out.println("[YoolooServer] Anzahl verbundene Spieler: " + clientHandlerList.size());
				} catch (IOException e) {
					System.out.println("Client Verbindung gescheitert");
					e.printStackTrace();
				}

				// Neue Session starten wenn ausreichend Spieler verbunden sind!
				if (clientHandlerList.size() >= Math.min(spielerProRunde,
						YoolooKartenspiel.Kartenfarbe.values().length)) {
					// Init Session
					YoolooSession yoolooSession = new YoolooSession(clientHandlerList.size(), serverGameMode);

					// Starte pro Client einen ClientHandlerTread
					for (int i = 0; i < clientHandlerList.size(); i++) {
						YoolooClientHandler ch = clientHandlerList.get(i);
						ch.setHandlerID(i);
						ch.joinSession(yoolooSession);
						spielerPool.execute(ch); // Start der ClientHandlerThread - Aufruf der Methode run()
						
					}

					// nuechste Runde eroeffnen

					clientHandlerList = new ArrayList<YoolooClientHandler>();
					AktuelleSpieler = new CopyOnWriteArrayList<String>();
				}
			}
		} catch (IOException e1) {
			System.out.println("ServerSocket nicht gebunden");
			serverAktiv = false;
			e1.printStackTrace();
		}

	}

	// TODO Dummy zur Serverterminierung noch nicht funktional
	public void shutDownServer(int code) {
		if (code == 543210) {
			this.serverAktiv = false;
			System.out.println("Server wird beendet");
			spielerPool.shutdown();
		} else {
			System.out.println("Servercode falsch");
		}
	}

	public void SaveKnownPlayers(ConcurrentHashMap<String, YoolooKarte[]> BekannteSpieler) {
		try {
			for (Entry<String, YoolooKarte[]> e : BekannteSpieler.entrySet()) {
				YoolooKarte[] Sortierung = (YoolooKarte[]) e.getValue();
				for (YoolooKarte karte : Sortierung) {
					if (karte == null) {
						BekannteSpieler.remove(e.getKey());
						break;
					}
				}
			}
			
			File file = new File("BekannteSpieler.save");
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(BekannteSpieler);
			oos.flush();
			oos.close();
			fos.close();
			System.out.println("Bekannte Spieler wurden gespeichert.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private ConcurrentHashMap<String, YoolooKarte[]> getKnownPlayersFromFile()
	{
		try {
			File toRead=new File("BekannteSpieler.save");
			FileInputStream fis=new FileInputStream(toRead);
			ObjectInputStream ois=new ObjectInputStream(fis);
	
			ConcurrentHashMap<String,YoolooKarte[]> BekannteSpieler = (ConcurrentHashMap<String,YoolooKarte[]>) ois.readObject();
	
			ois.close();
			fis.close();
			System.out.println("Eingelesene, bekannte Spieler:");
			for(Map.Entry<String,YoolooKarte[]> m :BekannteSpieler.entrySet()){
				System.out.println(m.getKey()+" : " + Arrays.toString(m.getValue()));
			}
			return BekannteSpieler;
		} 
		catch(Exception e) {
			//e.printStackTrace();
			BekannteSpieler = new ConcurrentHashMap<>();
			return BekannteSpieler;			
		}
	  
	}

}
